resource "aws_vpc" "my-main-network" {
  cidr_block = "10.0.0.0/16"

  tags = {
    Name = "my_main_network"
  }
}

resource "aws_subnet" "main-network-subnet-pub-1" {
  vpc_id                  = aws_vpc.my-main-network.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = "true"
  availability_zone       = "${var.aws_region}a"

  tags = {
    Name = "main-network-subnet-pub-1"
  }
}

/* Security Groups */
resource "aws_security_group" "allow-ssh" {
  vpc_id      = aws_vpc.my-main-network.id
  name        = "allow-ssh"
  description = "Allows connections on port 80"

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 22
    protocol    = "tcp"
    to_port     = 22
  }
}

resource "aws_security_group" "allow-tls" {
  vpc_id      = aws_vpc.my-main-network.id
  name        = "allow-tls"
  description = "Allows connections on port 443"

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
  }
}

resource "aws_security_group" "allow-80" {
  vpc_id      = aws_vpc.my-main-network.id
  name        = "allow-80"
  description = "Allows connections on port 80"

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
  }
}

resource "aws_security_group" "allow-8080" {
  vpc_id      = aws_vpc.my-main-network.id
  name        = "allow-8080"
  description = "Allows connections on port 8080"

  ingress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 8080
    protocol    = "tcp"
    to_port     = 8080
  }

  egress {
    cidr_blocks = ["0.0.0.0/0"]
    from_port   = 8080
    protocol    = "tcp"
    to_port     = 8080
  }
}


# create internet gateway so that things in private subnet can then be connected to the internet
resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = aws_vpc.my-main-network.id

  tags = {
    Name = "internet-gateway"
  }
}

# create routing from pub subnet to internet gateway
resource "aws_route_table" "public_ig_route_table" {
  vpc_id = aws_vpc.my-main-network.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }

  tags = {
    Name = "public_ig_route_table"
  }
}

resource "aws_route_table_association" "public_ig_route_association" {
  subnet_id      = aws_subnet.main-network-subnet-pub-1.id
  route_table_id = aws_route_table.public_ig_route_table.id
}