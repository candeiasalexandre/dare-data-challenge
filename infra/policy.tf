# Kinesis Firehose S3 write
data "aws_iam_policy_document" "kinesis_firehose_stream_assume_role" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["firehose.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "kinesis_firehose_access_bucket_assume_policy" {
  statement {
    effect = "Allow"

    actions = [
      "s3:AbortMultipartUpload",
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads",
      "s3:PutObject",
    ]

    resources = [
      aws_s3_bucket.tracking_api_bucket.arn,
      "${aws_s3_bucket.tracking_api_bucket.arn}/*",
    ]
  }
}

resource "aws_iam_role" "kinesis_firehose_stream_role" {
  name               = "kinesis_firehose_stream_role"
  assume_role_policy = data.aws_iam_policy_document.kinesis_firehose_stream_assume_role.json
}

resource "aws_iam_role_policy" "kinesis_firehose_access_bucket_policy" {
  name   = "kinesis_firehose_access_bucket_policy"
  role   = aws_iam_role.kinesis_firehose_stream_role.name
  policy = data.aws_iam_policy_document.kinesis_firehose_access_bucket_assume_policy.json
}

# EC2 roles to write kinesis firehose
data "aws_iam_policy_document" "ec2_assume_policy" {
  statement {
    effect  = "Allow"
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}
data "aws_iam_policy_document" "kinesis_firehose_write_assume_policy" {
  statement {
    effect = "Allow"

    actions = [
      "firehose:PutRecord",
    ]

    resources = [
      aws_kinesis_firehose_delivery_stream.tracking_api_stream.arn
    ]
  }
}

resource "aws_iam_role" "kinesis_write_role" {
  name               = "kinesis_write_role"
  assume_role_policy = data.aws_iam_policy_document.ec2_assume_policy.json
}

resource "aws_iam_role_policy" "kinesis_write_role_policy" {
  name   = "kinesis_firehose_write_delivery_stream_policy"
  role   = aws_iam_role.kinesis_write_role.name
  policy = data.aws_iam_policy_document.kinesis_firehose_write_assume_policy.json
}

resource "aws_iam_instance_profile" "kinesis_usage_profile" {
  name = "kinesis_usage_profile"
  role = aws_iam_role.kinesis_write_role.name
}