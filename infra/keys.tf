resource "tls_private_key" "private_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "ssh_key" {
  key_name   = var.ssh_key_name
  public_key = tls_private_key.private_key.public_key_openssh
}

resource "local_file" "save_ssh_key" {
  content         = tls_private_key.private_key.private_key_pem
  filename        = "${var.keys_path}/${var.ssh_key_name}.pem"
  file_permission = "0600"
}