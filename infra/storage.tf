resource "aws_s3_bucket" "tracking_api_bucket" {
  bucket = "tracking-api-${var.env_var}"
  acl    = "private"

  tags = {
    Name        = "Tracking Api Bucket"
    Environment = var.env_var
  }
}