variable "aws_region" {
  description = "AWS region value"
  type        = string
  default     = "eu-west-2"
}
variable "env_var" {
  description = "Env variable"
  type        = string
  default     = "dev"
}
variable "ssh_key_name" {
  default = "aws_tests_key"
}
variable "keys_path" {
  default = "/home/acandeias/.ssh"
}