resource "aws_kinesis_firehose_delivery_stream" "tracking_api_stream" {
  name        = "tracking_api_stream_${var.env_var}"
  destination = "s3"

  s3_configuration {
    role_arn        = aws_iam_role.kinesis_firehose_stream_role.arn
    bucket_arn      = aws_s3_bucket.tracking_api_bucket.arn
    buffer_interval = 60
  }

  tags = {
    "Name"        = "Tracking Api Delivery stream"
    "Environment" = var.env_var
  }
}