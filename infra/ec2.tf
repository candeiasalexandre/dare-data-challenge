resource "aws_instance" "tracking_api_server" {
  ami           = "ami-085ea8b418bf61ccd"
  instance_type = "t2.micro"
  key_name      = aws_key_pair.ssh_key.key_name
  vpc_security_group_ids = [
    aws_security_group.allow-ssh.id,
    aws_security_group.allow-8080.id,
    aws_security_group.allow-tls.id,
    aws_security_group.allow-80.id
  ]
  subnet_id = aws_subnet.main-network-subnet-pub-1.id

  iam_instance_profile = aws_iam_instance_profile.kinesis_usage_profile.name

  tags = {
    Name = "tracking_api_server_${var.env_var}"
  }
}