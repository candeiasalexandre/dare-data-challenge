import os
import pytest

os.environ["TRACKING_API_DELIVERY_STREAM_NAME"] = "tracking_api_stream_dev"

@pytest.fixture
def tracking_server_ip() -> str:
    """
    Gets the ip of the tracking_server machine from the hosts file
    """
    with open("ansible/hosts", 'r') as hosts_file:
        lines = hosts_file.readlines()
        for idx, line in enumerate(lines):
            if '[tracking-server]' in line:
                return lines[idx+1].rstrip("\n")