import pytest
from typing import Dict

from tracking_server.models.alias_body import AliasBody  # noqa: E501
from tracking_server.data_writing import DeliveryStreamWriter
import sys

@pytest.fixture
def dummy_data_size_kb() -> float:
    return 10

@pytest.fixture
def dummy_data(dummy_data_size_kb) -> Dict:
    return {
        'dummy_data': "alexandre"
    }

@pytest.fixture
def delivery_stream_writter() -> DeliveryStreamWriter:
    return DeliveryStreamWriter.get_instance()

def test_kinesis_write(dummy_data, delivery_stream_writter) -> None:
    """
    Tests data writting on kinesis stream.
    To check if the data is actually written u should check the S3 bucket.

    Asserts if the write data function returns true.
    """

    assert delivery_stream_writter.write_data(dummy_data)
