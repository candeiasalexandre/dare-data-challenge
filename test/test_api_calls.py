import pytest
import requests
import time

class TrackingServerSimpleClient():
    def __init__(self, tracking_server_ip):
        super().__init__()

        self._track_endpoint_url = f"{tracking_server_ip}:8080/track"
        self._alias_endpoint_url = f"{tracking_server_ip}:8080/alias"
        self._profile_endpoint_url = f"{tracking_server_ip}:8080/profile"

    def post_track(self, track_object: dict) -> str:

        r = requests.post(f"http://{self._track_endpoint_url}", json=track_object)
        return r.text

    def post_alias(self, alias_object: dict) -> str:

        r = requests.post(f"http://{self._alias_endpoint_url}", json=alias_object)
        return r.text

    def post_profile(self, profile_object: dict) -> str:

        r = requests.post(f"http://{self._profile_endpoint_url}", json=profile_object)
        return r.text

@pytest.fixture
def tracking_server_client(tracking_server_ip) -> TrackingServerSimpleClient:
    print(tracking_server_ip)
    return TrackingServerSimpleClient(tracking_server_ip)

@pytest.fixture
def track_test_object() -> dict:
    return {
        "userId": "test1",
        "events": [
            {
                "eventName": "test",
                "metadata": {
                    "pessoa": "alex",
                    "nome": "alex"
                },
                "timestampUTC": int(time.time())
            }
        ]
    }

@pytest.fixture
def alias_test_object() -> dict:
    return {
        "newUserId": "test2",
        "originalUserId": "test2",
        "timestampUTC": int(time.time())
    }

@pytest.fixture
def profile_test_object() -> dict:
    return {
        "userId": "test3",
        "attributes": {
            "pessoa": "alex",
            "nome": "alex"
        },
        "timestampUTC": int(time.time())
    }

def test_track_endpoint_call(tracking_server_client: TrackingServerSimpleClient, track_test_object: dict) -> None:

    assert tracking_server_client.post_track(track_test_object) == "true\n"

def test_alias_endpoint_call(tracking_server_client: TrackingServerSimpleClient, alias_test_object: dict) -> None:

    assert tracking_server_client.post_alias(alias_test_object) == "true\n"

def test_profile_endpoint_call(tracking_server_client: TrackingServerSimpleClient, profile_test_object: dict) -> None:
    
    assert tracking_server_client.post_profile(profile_test_object) == "true\n"