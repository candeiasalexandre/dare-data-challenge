SHELL := /bin/bash

create_dev_env:
	python3 -m venv env && \
	source env/bin/activate && \
	pip install -r src/api-server/requirements.txt && \
	pip install -r src/api-server/test-requirements.txt && \
	pip install -r src/api-server/build-requirements.txt && \
	pip install -e src/api-server

clean_dev_env:
	rm -r env

build:
	source env/bin/activate && \
	cd src/api-server && \
	python3 setup.py bdist_wheel --universal

clean_build:
	rm -r src/api-server/build & \
	rm -r src/api-server/dist & \
	rm -r src/api-server/tracking_server.egg-info

clean: clean_dev_env clean_build

deploy:
	cd ansible && \
	ansible-playbook tracking-server-setup.yml

run-api-call-tests:
	source env/bin/activate && \
	pytest test/test_api_calls.py