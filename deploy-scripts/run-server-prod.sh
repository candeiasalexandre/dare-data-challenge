#!/bin/bash

sudo pkill tracking_server
source /home/ubuntu/venv/bin/activate
export TRACKING_API_DELIVERY_STREAM_NAME=tracking_api_stream_dev
export AWS_DEFAULT_REGION=eu-west-2

nohup tracking_server_start > /home/ubuntu/tracking_api_server.log.err 2>&1 &