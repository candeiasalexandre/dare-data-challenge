from typing import Optional
from threading import Lock
import os
import json

import boto3

class DeliveryStreamWriter:
    """
    Class that represents a AWS Kinesis FireHose DeliveryStream writter for the Tracking API
    It writes data to the DeliveryStream name present in the ENV variable TRACKING_API_DELIVERY_STREAM_NAME
    """
    
    _instance: Optional['DeliveryStreamWriter'] = None
    _instance_creation_lock: Lock = Lock()

    def __init__(self) -> None:

        if self._instance is not None:
            raise Exception("This class is a singleton!")
        
        DeliveryStreamWriter._instance = self
        self._firehose_client = boto3.client("firehose")
        self._delivery_stream_name = os.environ['TRACKING_API_DELIVERY_STREAM_NAME']
    
    @classmethod
    def get_instance(cls) -> 'DeliveryStreamWriter':
        """
        Gets the instance of this class.
        """
        
        if cls._instance is None:
            with cls._instance_creation_lock:
                if cls._instance is None:
                    return DeliveryStreamWriter()
        
        return cls._instance
    
    def write_data(self, data: dict) -> bool:
        """
        Writes the provided dictionary to the delivery stream

        Args:
            data: dict with the data to be written in json format.

        Returns:
            Bool True if data could be written False otherwise.
        """

        output = self._firehose_client.put_record(
            DeliveryStreamName=self._delivery_stream_name,
            Record={
                'Data': json.dumps(data).encode('utf-8')
            }
        )
        return output["ResponseMetadata"]["HTTPStatusCode"] == 200
