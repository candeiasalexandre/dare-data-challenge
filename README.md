# dare-ingestion-api

Repository containing DareData Data Engineer Challenge.

This repo implements:
- REST API based on the swagger specification present in [here](src/api-server/tracking_server/swagger/swagger.yaml)
- Automation of deployment for the API server based on Terraform and Ansible

The rest API consists of a very simple solution to log json data in a S3 bucket using a Kinesis stream.

## Build Notes
Check the Makefile present, it has a target to create a environment and to build a wheel containing the code of the api-server.

## Deployment Notes

0. Build a wheel for the api server by doing `make create_dev_env` and `make build`.
1. Go to infra folder.
2. Check the `variables.tf` file and change the location where you want the ssh key that provides access to your ec2 machine to be saved, the default is /home/acandeias/.ssh.
3. Run `terraform apply` and wait for the deployment to be done.
4. Use `terraform show` to get the ec2 instance public ip.
5. Add the ssh key that was created to your ssh-agent by using `ssh-add path_for_the_key`.
6. Add the public ip to the `ansible/hosts` file under the tracking-server tag.
7. Go back to the root of the repo. 
8. Run `make deploy` to apply the ansible playbook to the ec2 machine you just created
9. Your server should be up and running on port 8080. You can test by using curl or postman to post things on the api endpoints, after 60 seconds (Kinesis Buffer policy you should see your requests written on S3 bucket tracking-api-dev).

**DON'T FORGET TO DESTROY THE INFRA THAT YOU CREATED USING TERRAFORM BY DOING**
``terraform destroy``

## Tests

To check if the deployment was successful and the server is answering correctly run the `make run-api-call-tests` which will try to post dummy data at each of the endpoints
and asserts the server response ( which should be `true`). You should check on aws console if the files were written after ~60 seconds.

## Points of Improvement


- Add a different delivery stream for each of the endpoints, currently I only use one delivery stream for all the endpoints, this makes it difficult to realize which files come from each of the endpoints, it also makes that sometimes multiple calls are written to the same file on s3. With different streams we could use different prefixes on the s3 files.

- Change deployment server from flask development server to a production ready server.

- Add tests to actually assert the files that were written to s3.

## Bugs

- Sometimes we aggregate multiple endpoint files on the same s3 file, this is probably due to the fact of using the same delivery stream.
